require_relative('../rspec_helper')

describe ConfigService do
  context 'load_config' do
    it 'loads existing config file' do
      config = ConfigService.load_config('memcached.yml')
      expect(config).not_to be_nil
      expect(config.is_a? OpenStruct).to eql(true)
    end

    it 'loads existing config file with ERB template correctly' do
      ENV['TEST_ERB'] = 'test erb'
      config = ConfigService.load_config('memcached.yml')
      expect(config['test']['namespace']).to eql('test erb')
    end

    it 'loads existing config file with full path correctly' do
      full_path = "#{File.expand_path('.')}/conf/memcached.yml"
      puts full_path
      config = ConfigService.load_config(full_path)
      expect(config).not_to be_nil
      expect(config.is_a? OpenStruct).to eql(true)
    end

    it 'raises error if the conf directory and config directory do not exist or the config file does not exist' do
      allow(File).to receive(:exist?).and_return(false)
      expect {ConfigService.load_config('cache_config.yml')}.to raise_error RuntimeError
    end
  end

  context 'environment' do
    after :each do
      ENV['RACK_ENV'] = 'test'
      ENV['ENV']      = 'test'
    end

    it 'returns test by default while running test' do
      expect(ConfigService.environment).to eql('test')
    end

    it 'returns development if the environment is not set' do
      ENV['RACK_ENV'] = nil
      ENV['ENV']      = nil
      expect(ConfigService.environment).to eql('development')
    end

    it 'returns correct value based on environment variable' do
      ENV['RACK_ENV'] = 'dummy'
      expect(ConfigService.environment).to eql('dummy')
    end

    it 'returns ENV variable value if RACK_ENV is not set' do
      ENV['RACK_ENV'] = nil
      ENV['ENV']      = 'env_value'
      expect(ConfigService.environment).to eql('env_value')
    end
  end
end
